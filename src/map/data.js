const cardData = [
    { id: 1, title: "Anusha Plumber ", description: "This is a dummy description about this shop", rating: 5 },
    { id: 2, title: "Anvesha Electricity ", description: "This is a dummy description about this shop", rating: 4 },
    { id: 3, title: "Nepal Furni Shop ", description: "This is a dummy description about this shop", rating: 2 },
    { id: 4, title: "Anisha Electricity ", description: "This is a dummy description about this shop", rating: 3 },
    { id: 5, title: "Limbu Plumber", description: "This is a dummy description about this service provider ", rating: 3 },
  ];
  
  export default cardData;
  